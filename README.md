# appdev

Central repository for the i-CreatorZ Club App Development Interest Group.

If you're interest to join and gain write access to the current repo, please inform the high committees in the Discord server.

When you're submitting patches or project codes to the current repo, please make sure you abide the Code of Conduct (`CODE-OF-CONDUCT.MD`).
